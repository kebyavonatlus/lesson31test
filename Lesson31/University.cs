﻿using System.Collections.Generic;
using System.Linq;

namespace Lesson31
{
    public class University<T> where T : Person
    {
        public List<T> Persons = new List<T>(); // null

        public List<T> GetStudentsByMaritalStatus(MaritalStatus status)
            {
            return Persons
                .Where(x => x.MaritalStatus == status && x.Position == Position.Student)
                .ToList();
        }

        public List<T> GetStudentsByGender(Gender gender)
        {
            return Persons
                .Where(x => x.Gender == gender && x.Position == Position.Student)
                .ToList();
        }

        public List<T> GetStudentsByGenderAndMS(Gender gender, MaritalStatus status)
        {
            return Persons.Where(x => x.MaritalStatus == status && x.Gender == gender && x.Position == Position.Student).ToList();
        }

        public List<T> GetAllStudents()
        {
            return Persons.Where(x => x.Position == Position.Student).ToList();
        }
    }
}
