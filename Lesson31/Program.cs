﻿using System;
using System.Collections.Generic;

namespace Lesson31
{
    class Program
    {
        static void Main(string[] args)
        {
            var persons = new List<Person> {
                Person.Generate("Alex")
            };

            var university = new University<Person>();

            university.Persons.AddRange(persons);

            var marital = university.GetAllStudents();
            Print(marital);
        }

        public static void Print(List<Person> persons)
        {
            foreach (var person in persons)
                Console.WriteLine(person);

            Console.WriteLine();
        }
    }
}
